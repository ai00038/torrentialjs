(function() {
	'use strict';
	
	angular
		.module('main')
			.controller('MainController', MainController);
			
	function MainController($resource){
		var vm = this,
			baseUrl = 'http://api.openweathermap.org/data/2.5/',
			callbackFormat = { callback: 'JSON_CALLBACK' },
			getMethod = { get: { method: 'JSONP' }},
			apiKey = '75ab4a21554c3eea638f0493fc501db1';
		
		vm.handlePresent = handlePresent;
		vm.handleFuture = handleFuture;
		
		function handlePresent(req){
			var weatherAPI = $resource(baseUrl+'weather', callbackFormat, getMethod);
			vm.presentData = null;
			vm.loading = true;
			weatherAPI
				.get({ q: req.location, APPID: apiKey })
				.$promise
				.then(function (data){
					vm.presentData = data;
				})
				.finally(function (){
					vm.loading = false;
				});
		}
		
		function handleFuture(req){
			var weatherAPI = $resource(baseUrl+'forecast', callbackFormat, getMethod);
			vm.futureData = null;
			vm.loading = true;
			weatherAPI
				.get({ q: req.location, APPID: apiKey })
				.$promise
				.then(function (data){
					vm.futureData = data;
				})
				.finally(function (){
					vm.loading = false;
				});
		}
		
	}
	
})();