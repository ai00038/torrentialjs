(function (){
	
	angular
		.module('d3Utils')
			.directive('d3LinearChart', d3LinearChart);
			
	function d3LinearChart() {
		
		var directive = {
			restrict: 'A',
			template: '<svg width="750" height="500"></svg>',
			link: link
		};
		
		function link(scope, elem, attrs){
			var dataToPlot = scope[attrs.chartData],
				padding = 20,
				pathClass = 'path',
				xScale, yScale, xAxisGen, yAxisGen, lineFun,
				rawSvg = elem.find('svg')[0],
				svg = d3.select(rawSvg);
			
			function setChartParameters(){
			  console.log(scope);
			  xScale = d3.scale.linear()
						 .domain([dataToPlot[0].hour, dataToPlot[dataToPlot.length - 1].hour])
						 .range([padding + 5, rawSvg.clientWidth - padding]);

			  yScale = d3.scale.linear()
				.domain([0, d3.max(dataToPlot, function (d) {
				  return d.sales;
				})])
				.range([rawSvg.clientHeight - padding, 0]);

			  xAxisGen = d3.svg.axis()
						   .scale(xScale)
						   .orient('bottom')
						   .ticks(dataToPlot.length - 1);

			  yAxisGen = d3.svg.axis()
						   .scale(yScale)
						   .orient('left')
						   .ticks(5);

			  lineFun = d3.svg.line()
						  .x(function (d) {
							return xScale(d.hour);
						  })
						  .y(function (d) {
							return yScale(d.sales);
						  })
						  .interpolate('basis');
			}
					 
			function drawLineChart() {
			  setChartParameters();

			  svg.append('svg:g')
				 .attr('class', 'x axis')
				 .attr('transform', 'translate(0,180)')
				 .call(xAxisGen);

			   svg.append('svg:g')
				  .attr('class', 'y axis')
				  .attr('transform', 'translate(20,0)')
				  .call(yAxisGen);

			   svg.append('svg:path')
				  .attr({
					d: lineFun(dataToPlot),
					'stroke': 'blue',
					'stroke-width': 2,
					'fill': 'none',
					'class': pathClass
			   });
			}

			drawLineChart();
		}
		
		return directive;
	}
	
})();